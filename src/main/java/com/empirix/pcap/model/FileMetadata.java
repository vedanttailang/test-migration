package com.empirix.pcap.model;

import java.time.OffsetDateTime;

public class FileMetadata {

	private String requestId;
	private String fileType;
	private String fileName;
	private String status;
	private boolean visibility;
	private Integer savedResultId;
	private String userName;
	private OffsetDateTime createdDate;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isVisibility() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	public Integer getSavedResultId() {
		return savedResultId;
	}

	public void setSavedResultId(Integer savedResultId) {
		this.savedResultId = savedResultId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public OffsetDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(OffsetDateTime createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileMetadata [requestId=");
		builder.append(requestId);
		builder.append(", fileType=");
		builder.append(fileType);
		builder.append(", fileName=");
		builder.append(fileName);
		builder.append(", status=");
		builder.append(status);
		builder.append(", visibility=");
		builder.append(visibility);
		builder.append(", savedResultId=");
		builder.append(savedResultId);
		builder.append(", userName=");
		builder.append(userName);
		builder.append(", createdDate=");
		builder.append(createdDate);
		builder.append("]");
		return builder.toString();
	}

}
