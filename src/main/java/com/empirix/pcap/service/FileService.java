package com.empirix.pcap.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class FileService {

	@ConfigProperty(name = "packetreader.response.base.location")
	String baseLocation;

	private static final Logger LOGGER = LoggerFactory.getLogger(FileService.class);

	public List<String> getFileList() {

		List<String> fileList ;
		List<String> allFileList = new ArrayList<>();
		

		File[] directories = getAllDirectories(Paths.get(baseLocation));

		for (File directory : directories) {

			Path directoryPath = Paths.get(directory.toString());

			try {

				fileList = listFiles(directoryPath);
				allFileList.addAll(fileList);
			} catch (IOException e) {
				LOGGER.error("{}", e.getMessage());
			}

		}
		return allFileList.stream().filter(s->!s.startsWith(".")).collect(Collectors.toList());

	}

	public List<String> listFiles(Path path) throws IOException {
		try (Stream<Path> list = Files.list(path)) {
			return list.filter(p -> p.toFile().isFile()).filter(p->!p.getFileName().startsWith(".")).map(s->s.getFileName().toString()).collect(Collectors.toList());
		}

	}
	
	

	public File[] getAllDirectories(Path baselocation) {

		List<File> directories = new ArrayList<>();
		if (baselocation.toFile().exists()) {
			File dp = new File(baselocation.toString());

			for (File file : dp.listFiles()) {
				if (file.isDirectory()) {
					directories.add(file);
				}
			}
		}
		return directories.toArray(new File[directories.size()]);
	}
}
