package com.empirix.pcap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.empirix.pcap.model.FileMetadata;
import com.empirix.pcap.repository.FileRepository;
import com.empirix.pcap.service.FileService;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;

@QuarkusMain
public class MigrationService {

//	public static void main(String[] args) {
//		
//		String file = "40c85f70f67a4643996c6ea021a22854_SIPProtocol_COMPLETED_FALSE_1621265540952.txt";
//		String [] files = file.split("_");
//		String fileName = files[0];
//		String creationTime = file.substring(file.lastIndexOf("_")+1);
//		String ext = creationTime.substring(creationTime.indexOf(".")+1);
//		
//		System.out.println(fileName);
//		System.out.println(creationTime);
//		System.out.println(ext);
//		
//		System.out.println("Running main method");
//		
//		fileRepository.
//        Quarkus.run(args); 
//	}

	public static void main(String... args) {
		Quarkus.run(MigratioApp.class, args);
	}

	public static class MigratioApp implements QuarkusApplication {

		@Inject
		FileRepository fileRepository;

		@Inject
		FileService fileService;

		@ConfigProperty(name = "packetreader.response.base.location")
		String baseLocation;

		@Inject
		PgPool client;

		private static final Logger LOGGER = LoggerFactory.getLogger(MigratioApp.class);

		@Override
		public int run(String... args) throws Exception {
			LOGGER.info("====Start UP======");
			System.out.println();
			System.out.println("=========================");

			// Step 1 : List out the files in opt-pcap storage
			List<String> filesInDir = fileService.getFileList();

			System.out.println("Total files in opt/pcap " + filesInDir.size());

			System.out.println("Files in opt/pcap-storage :");

			for (String string : filesInDir) {
				System.out.println(string);
			}

			List<String> requestIdsWithUnderScore = filesInDir.stream().filter(s -> s.contains("_"))
					.collect(Collectors.toList());

			System.out.println();
			System.out.println("=========================with underscore===files==");
			for (String s : requestIdsWithUnderScore) {

				System.out.println(s);

			}
			System.out.println();
			System.out.println("=========================");

			// Step 2 : List out the files in database (pcap-metadata)

			Uni<List<FileMetadata>> uni = fileRepository.findAll(client).collect().asList();

			List<FileMetadata> fileListss = uni.await().indefinitely();

			System.out.println("Total files in database is  " + fileListss.size());
			List<String> requestIds = fileListss.stream().map(s -> s.getRequestId()).collect(Collectors.toList());

			for (String string : requestIds) {
				System.out.println(string);
			}

			System.out.println();
			// Step 3 : List out the files which are not present in database but in opt-pcap
			// storage.

			String file = "40c85f70f67a4643996c6ea021a22854_SIPProtocol_COMPLETED_FALSE_1621265540952.txt";

			// System.out.println(file.indexOf("."));

			// System.out.println("File name after subs
			// :"+file.substring(0,file.indexOf(".")));

			System.out.println();
			System.out.println("=========================");

			System.out.println("Missing files in database that needs to be restored");
			List<String> requestIdsinFilePaths = filesInDir.stream().map(s -> s.split("_")[0])
					.filter(s -> s.contains(".")).map(s -> (s.substring(0, s.indexOf("."))))
					.collect(Collectors.toList());

			// requestIdsinFilePaths.replaceAll(s->(s.substring(s.indexOf(".")),"");

//			requestIdsinFilePaths.stream().map(s->(s.substring(0, s.indexOf("."))))
//				.collect(Collectors.toList());
			System.out.println();
			System.out.println("=========================");
			for (String string : requestIdsinFilePaths) {
				System.out.println(string);
			}
			System.out.println("RequestIds  filepath and size is " + requestIdsinFilePaths.size());
			// System.out.println("Removing files
			// "+requestIdsinFilePaths.removeAll(requestIds));

			// System.out.println(requestIdsinFilePaths);
			// System.out.println("Total files needs to restored :
			// "+requestIdsinFilePaths.size());

			// Step up : Insert the missing file meta data in pcap-metadata table.

			// Getting SavedResultID from file logic
			/*
			 * 1.Check the file which has underscore 
			 * 
			 * 2.Split the filename with underscore and convert it in Array. 
			 * 
			 * 3.Reverse the array in List 
			 * 
			 * 4.Check with 2nd index is String or integer
			 *  
			 * 5.If its integer then it is SAvedResultId and save this ID in DB 
			 * 
			 * 6.If its not integer then save this as 0 (zero)
			 */

			String testFile = "40c85f70f67a4643996c6ea021a22854_SIPProtocol_COMPLETED_FALSE_1621265540952.txt";

			List<String> myListWithoutId = new ArrayList<String>(Arrays.asList(testFile.split("_")));
			
						Collections.reverse(myListWithoutId);

			System.out.println("Reversed Array:" + myListWithoutId);

			String testFileWithID = "40c85f70f67a4643996c6ea021a22854_SIPProtocol_COMPLETED_100_FALSE_1621265540952.txt";


			List<String> myListWithId = new ArrayList<String>(Arrays.asList(testFileWithID.split("_")));
			
			Collections.reverse(myListWithId);
			
			System.out.println("Reversed Array with Saved ID:" + myListWithId);

			int savedId = 0;
			System.out.println(" Index at 3 value with ID : " + myListWithId.get(2));
			

			System.out.println(" Index at 3 value without ID : " + myListWithoutId.get(2));
			
			//Checking if its number or string 
			Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
			
			if(pattern.matcher(myListWithId.get(2)).matches())
			{
				System.out.println("IS number "+myListWithId.get(2));
			}
			
			else
			{
				System.out.println("Not a number "+myListWithId.get(2));
			}
			
			if(pattern.matcher(myListWithoutId.get(2)).matches())
			{
				System.out.println("IS number "+myListWithoutId.get(2));
			}
			
			else
			{
				System.out.println("Not a number "+myListWithoutId.get(2));
			}
			
			//If its a number then we will save this in SAvedResultId columns

			Quarkus.waitForExit();
			return 0;

		}
	}
}
