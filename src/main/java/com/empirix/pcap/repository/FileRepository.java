package com.empirix.pcap.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.empirix.pcap.model.FileMetadata;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.pgclient.PgPool;

@ApplicationScoped
public class FileRepository {

	@Inject
	PgPool client;

	private static final String GET_FILE_METADATA_ALL_FILES = "SELECT request_id, file_type, file_name, status, visibility, saved_result_id, user_name, created_date "
			+ "FROM pcap_library.pcap_metadata";

	public Multi<FileMetadata> findAll(PgPool client) {
		return client.query(GET_FILE_METADATA_ALL_FILES).execute().onItem()
				.transformToMulti(set -> Multi.createFrom().iterable(set)).onItem()
				.transform(s -> buildFileMetadata(s));
	}

	private FileMetadata buildFileMetadata(io.vertx.mutiny.sqlclient.Row item) {
		FileMetadata fileMetadata = new FileMetadata();
		fileMetadata.setRequestId(item.getString("request_id"));
		fileMetadata.setFileType(item.getString("file_type"));
		fileMetadata.setFileName(item.getString("file_name"));
		fileMetadata.setStatus(item.getString("status"));
		fileMetadata.setVisibility(item.getBoolean("visibility"));
		fileMetadata.setSavedResultId(item.getInteger("saved_result_id"));
		fileMetadata.setUserName(item.getString("user_name"));
		fileMetadata.setCreatedDate(item.getOffsetDateTime("created_date"));
		return fileMetadata;
	}

}
